# Kontena local Demo

Demo of Kontena using docker-compose. Used to quickly trial Kontena.

## Requirements

- docker
- docker-compose
- kontena-cli (https://www.kontena.io/docs/tools/cli.html)

## Installation

```bash
docker-compose up -d
kontena master login --code loginwiththiscodetomaster http://localhost:80

kontena grid create demo
kontena grid use demo
```

For more details, see the installation documentation: https://www.kontena.io/docs/advanced/install-master/docker-compose.html

## GitHub authentication

A kontena configuration file (`kontena.yml`) has been set up to configure everything for user authentication through GitHub OAuth2.
See Authentication documentation: https://www.kontena.io/docs/advanced/authentication.html
